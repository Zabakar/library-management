#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "library.h"

void owner_area(user_t *u) {
	int choice,memberid;
	do {
		printf("\n\n0. Sign Out\n1. Appoint Librarian\n2. Edit Profile\n3. Change Password\n4. Fees/Fine Report\n5. Book Availability\n6. Book Categories/Subjects\nEnter choice: ");
		scanf("%d", &choice);
		switch(choice) {
			case 1: // Appoint Librarian
				appoint_librarian();
				break;
			case 2://edit profile
                   editprofile();
				break;
			case 3://change pass
                   changepass();
				break;
			case 4://fees fine report
                   printf("enter member id of the member\n");
		           scanf("%d",&memberid);
		           payment_history(memberid);
				break;
			case 5://book Availability
                   bookcopy_checkavail_details();
                   break;
				break;
			case 6://book catagories/subjects
                   subjects();
                   break;
				break;
		}
	}while (choice != 0);	
}

void appoint_librarian() {
	// input librarian details
	user_t u;
	user_accept(&u);
	// change user role to librarian
	strcpy(u.role, ROLE_LIBRARIAN);
	// add librarian into the users file
	user_add(&u);
}


void subjects(){
       book_t b,nb;
       char subject[30];
       printf("Enter the subject to be search\n");
       scanf("%s",&subject);

	int found = 0;
	FILE *fp;
	
	fp = fopen(BOOK_DB, "rb+");
	if(fp == NULL) {
		perror("cannot open users file\n");
		exit(1);
	}
	while(fread(&b, sizeof(book_t), 1, fp) > 0) {
		if(strcmp(b.subject,subject)==0) {
			printf("book id: %d, name: %s, author: %s, subject: %s, price: %.2lf, isbn: %s\n",b.id,b.name,b.author,b.subject,b.price,b.isbn);
		}
	}
	
fclose(fp);}